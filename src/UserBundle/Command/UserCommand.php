<?php
// src/UserBundle/Command/UserCommand.php
namespace UserBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('user:list')
            ->setDescription('List users available');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$users = $this->getContainer()->get('doctrine')->getRepository('AppBundle:User')->findAll();
        $output->writeln($users);
    }
}
 
