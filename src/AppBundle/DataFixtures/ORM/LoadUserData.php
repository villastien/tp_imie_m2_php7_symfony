<?php 
// src/AppBundle/DataFixtures/ORM/LoadUserData.php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setUsername('admin');
        $user->setEmail('admin@imie.fr');
        $user->setPlainPassword('nimda');
        $user->setEnabled(true);

        $manager->persist($user);

        $user1 = new User();
        $user1->setUsername('totoro');
        $user1->setEmail('totoro@ghibli.jp');
        $user1->setPlainPassword('chihiro');
        $user1->setEnabled(true);

        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('borzaz');
        $user2->setEmail('borzaz@lannion.bzh');
        $user2->setPlainPassword('bornibus');
        $user2->setEnabled(true);

        $manager->persist($user2);

        $user3 = new User();
        $user3->setUsername('renge');
        $user3->setEmail('renge@manga.jp');
        $user3->setPlainPassword('Nyanpasu');
        $user3->setEnabled(true);

        $manager->persist($user3);
        $manager->flush();
    }
}
