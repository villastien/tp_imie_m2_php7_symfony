Exercice de l'école IMIE pour apprendre à utiliser symfony et docker.

Mail professeur : christopher.louet@imie-rennes.fr
Auteur de cette version : Sébastien VILLALON

Consignes :
1) Installation de FOS/User
2) Création d'un service (UserBundle\Schema\Schema)
3) Affichier les utilisateurs dans une vue
4) Afficher les utilisateurs dans une commande
5) Création de fixtures pour la classe utilisateur

Réponses :
2)
'src/UserBundle/Schema/Schema.php' contient le service showHelloWorld
3)
Utilisation de 'generate:doctrine:crud' afin de générer la vue contenant la liste des utilisateurs. Il est disponible sous 'http://ip:port/user' et les fichiers sont :
- 'src/AppBundle/Controller/UserController.php' avec comme fonction 'indexAction()'
- 'app/Resources/views/user/index.html.twig'
4) 
La commande 'user:list' est utilisé pour afficher la liste des utilisateurs. 
Cette commande est crée dans 'src/UserBundle/Command/UserCommand.php'
5)
Les fixtures sont crées dans 'src/AppBundle/DataFixtures/ORM/LoadUserData.php'
